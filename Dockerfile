FROM openjdk:10-jre-slim
VOLUME /tmp

EXPOSE 8080

ARG JAR_FILE=target/my-app-1.5-SNAPSHOT.jar

ADD ${JAR_FILE} app.jar

ENTRYPOINT ["java","-jar","/app.jar"]
